<?php

$EM_CONF[$_EXTKEY] = array(
    'title'              => 'BossharTong: simple charts',
    'description'        => 'a simple integration of highchart.js',
    'category'           => 'plugin',
    'version'            => '0.0.6',
    'shy'                => '',
    'priority'           => '',
    'loadOrder'          => '',
    'module'             => '',
    'state'              => 'stable',
    'uploadfolder'       => 0,
    'createDirs'         => '',
    'modify_tables'      => '',
    'clearcacheonload'   => 1,
    'lockType'           => '',
    'author'             => 'Nando Bosshart',
    'author_email'       => 'typo3@bosshartong.ch',
    'author_company'     => 'BossharTong GmbH',
    'CGLcompliance'      => null,
    'CGLcompliance_note' => null,
    'constraints'        => array(
        'depends' => array(
            'extbase' => '8.7',
            'fluid' => '8.7',
            'typo3' => '8.7',
        ),
        'conflicts' => array(
        ),
        'suggests' => array(
        ),
    ),
    'autoload' => array(
        'psr-4' => array(
            'Bosshartong\\BotoCharts\\' => 'Classes',
        ),
    ),
);
