<?php
if (!defined('TYPO3_MODE')) {
	die ('Access denied.');
}
// Include new content elements to modWizards
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig(
    '<INCLUDE_TYPOSCRIPT: source="FILE:EXT:boto_charts/Configuration/PageTSconfig/BotoCharts.ts">'
);

// Include TypoScript
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile($_EXTKEY, 'Configuration/TypoScript', 'boto Charts :: Highcharts');

// Add a flexform to the boto_charts CType
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue(
    '*',
    'FILE:EXT:boto_charts/Configuration/FlexForms/flexform.xml',
    'botocharts_chart'
);
