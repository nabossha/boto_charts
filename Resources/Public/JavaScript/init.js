/**
 * init.js
 *
 */
Boto = {};
Boto.Bootstrap = $.extend({}, {
    bootstrappers: [],
    bootstrap: function () {
        Boto.Bootstrap._invokeBootstrappers();
    },
    registerBootstrap: function (bootstrap) {
        Boto.Bootstrap.bootstrappers.push(bootstrap);
    },
    _invokeBootstrappers: function () {
        $.each(Boto.Bootstrap.bootstrappers, function (index, bootstrapper) {
            bootstrapper.initialize();
        });
    }
});

$(function () {
    Boto.Bootstrap.bootstrap();
});

Boto.ChartController = $.extend({}, {
    Configuration: [],
    initialize: function() {
        if (typeof botochartsArray !== 'undefined' && botochartsArray.length > 0) {
            jQuery.getScript("https://code.highcharts.com/highcharts.js", function( data, textStatus, jqxhr ) {
                    if(jqxhr.status === 200) {
                        Boto.ChartController.buildCharts();
                    }
                }
            );
        }
    },
    buildCharts: function () {
        // set default colors:
        Highcharts.setOptions({
            colors: ['#006FB7','#F8B322', '#7DB956', '#AAAB7C', '#67726B', '#5EB3E0', '#A06610', '#FFF263','#6AF9C4']
            });
        // initialize each chart within the page:
        botochartsArray.forEach(function(element) {
            Highcharts.setOptions(element['globalOptions']);
            var HighChart = "boto_charts_"+element['id'];
            HighChart = new Highcharts.Chart(element['chartOptions']);
        });
    }
});
if (typeof Boto.Bootstrap !== 'undefined') {
    Boto.Bootstrap.registerBootstrap(Boto.ChartController);
}
