<?php
namespace Bosshartong\BotoCharts\DataProcessing;

/*
 * This file is part of the TYPO3 CMS project.
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * The TYPO3 project - inspiring people to share!
 */

use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Frontend\ContentObject\ContentObjectRenderer;
use TYPO3\CMS\Frontend\ContentObject\DataProcessorInterface;
use TYPO3\CMS\Frontend\ContentObject\Exception\ContentRenderingException;

/**
 * This data processor will calculate rows, columns and dimensions for a gallery
 * based on several settings and can be used for f.i. the CType "hf_images"
 */
class BotoChartsProcessor implements DataProcessorInterface
{

    /**
     * Process data for the CType "boto_slider"
     *
     * @param ContentObjectRenderer $cObj The content object renderer, which contains data of the content element
     * @param array $contentObjectConfiguration The configuration of Content Object
     * @param array $processorConfiguration The configuration of this processor
     * @param array $processedData Key/value store of processed data (e.g. to be passed to a Fluid View)
     * @return array the processed data as key/value store
     * @throws ContentRenderingException
     */
    public function process(
        ContentObjectRenderer $cObj,
        array $contentObjectConfiguration,
        array $processorConfiguration,
        array $processedData
    ) {

        // prepare flexform-data for parsing:
        $flexformService = GeneralUtility::makeInstance('TYPO3\\CMS\\Extbase\\Service\\FlexFormService');
        $processedFlexFormData = $flexformService->convertFlexFormContentToArray($cObj->data['pi_flexform']);

        $chartidentifier = 'boto_chart_'.$cObj->data['uid'];

        // set all data series in the used array
        $a_allSeries = $processedFlexFormData['allSeries'];

        if ( isset( $a_allSeries ) && !empty( $a_allSeries ) ) {
            foreach ( $a_allSeries as $serie ) {
                if ( isset( $serie['xmlSeries'] ) ) {
                    $a_serieObjects[] = array (
                        'title' => $serie['xmlSeries']['serieTitle'],
                        'data' => $serie['xmlSeries']['serieData'],
                        'type' => $serie['xmlSeries']['serieType'],
                        'yAxis' => $serie['xmlSeries']['serieYaxis'],
                        'color' => $serie['xmlSeries']['serieColor'],
                        'dashStyle' => $serie['xmlSeries']['serieDashStyle'],
                        'additionalCode' => $serie['xmlSeries']['serieAdditional'],
                    );
                }
            }
        }

        // set vars for js
        $inverted  = ( $processedFlexFormData['inverted'] == 1 ) ? ',inverted: true':',inverted: false';
        $shadow  = ( $processedFlexFormData['shadow'] == 1 ) ? ',shadow: true':',shadow: false';
        $width   = ( $processedFlexFormData['width'] > 0 ) ? ',width: '.$processedFlexFormData['width']:'';
        $height   = ( $processedFlexFormData['height'] > 0 ) ? ',height: '.$processedFlexFormData['height']:'';
        $categoriesX = ($processedFlexFormData['xaxisCategories'] <> "") ? ", categories: ['".str_replace(",", "','", $processedFlexFormData['xaxisCategories'])."']" : ", categories: null";
        $categoriesYone = ($processedFlexFormData['yaxisOneCategories'] <> "") ? ", categories: ['".str_replace(",", "','", $processedFlexFormData['yaxisOneCategories'])."']" : ", categories: null";
        $categoriesYtwo = ($processedFlexFormData['yaxisTwoCategories'] <> "") ? ", categories: ['".str_replace(",", "','", $processedFlexFormData['yaxisTwoCategories'])."']" : ", categories: null";
        $type = ( $processedFlexFormData['type'] != '' ) ? ", type: '".$processedFlexFormData['type']."'" : '';
        $plotoptions = ( $processedFlexFormData['plotoptions'] != '' ) ? "plotOptions: { ".$processedFlexFormData['plotoptions']." }," : "";
        $legend = ( $processedFlexFormData['chartleg'] != '' ) ? "legend: { ".$processedFlexFormData['chartleg']." }," : "";
        $tooltip = ( $processedFlexFormData['tooltip'] != '' ) ? "tooltip: { ".$processedFlexFormData['tooltip']." }," : "";
        $setOptions = ( $processedFlexFormData['customcolors'] != '' ) ? "globalOptions:{".$processedFlexFormData['customcolors']."}," : "";

        $xaxisGridLineWidth = ( $processedFlexFormData['xaxisGridLineWidth'] > 0 ) ? ',gridLineWidth: '.$processedFlexFormData['xaxisGridLineWidth']:'';
        $yaxisOneGridLineWidth = ( $processedFlexFormData['yaxisOnwGridLineWidth'] > 0 ) ? ',gridLineWidth: '.$processedFlexFormData['yaxisOneGridLineWidth']:'';
        $yaxisTwoGridLineWidth = ( $processedFlexFormData['yaxisTwoGridLineWidth'] > 0 ) ? ',gridLineWidth: '.$processedFlexFormData['yaxisTwoGridLineWidth']:'';

        $xaxisTitleColor = ( $processedFlexFormData['xaxisTitleColor'] ) ? ",style: { color: '".$processedFlexFormData['xaxisTitleColor']."'}" : "";
        $yaxisOneTitleColor = ( $processedFlexFormData['yaxisOneTitleColor'] ) ? ",style: { color: '".$processedFlexFormData['yaxisOneTitleColor']."'}" : "";
        $yaxisTwoTitleColor = ( $processedFlexFormData['yaxisTwoTitleColor'] ) ? ",style: { color: '".$processedFlexFormData['yaxisTwoTitleColor']."'}" : "";

        $xaxisFormater = ( $processedFlexFormData['xaxisFormater'] ) ? ",labels: {".$processedFlexFormData['xaxisFormater']."}" : "";
        $yaxisOneFormater = ( $processedFlexFormData['yaxisOneFormater'] ) ? ",labels: {".$processedFlexFormData['yaxisOneFormater']."}" : "";
        $yaxisOneAdditional = ( $processedFlexFormData['yaxisOneAdditional'] ) ? ",".$processedFlexFormData['yaxisOneAdditional'] : "";
        $yaxisTwoFormater = ( $processedFlexFormData['yaxisTwoFormater'] ) ? ",labels: {".$processedFlexFormData['yaxisTwoFormater']."}" : "";
        $yaxisTwoAdditional = ( $processedFlexFormData['yaxisTwoAdditional'] ) ? ",".$processedFlexFormData['yaxisTwoAdditional'] : "";

        $jsCode = "$chartidentifier = {
                $setOptions
                id: '$chartidentifier',
			    chartOptions: {
			        chart: {
			            renderTo: '$chartidentifier'
			            $type
			            $inverted
			            $width
			            $height
			            $shadow
			        },
			        $plotoptions
			        $legend
			        $tooltip
			        credits: {
				        enabled: false
				    },
			        title: {
			            text: '".$processedFlexFormData['title']."'
			        },
			        subtitle: {
			            text: '".$processedFlexFormData['subtitle']."'
			        },
			        xAxis: {
			            title: {
			                text: '".$processedFlexFormData['xaxisTitle']."'
			                $xaxisTitleColor
			            }
			            $categoriesX
			            $xaxisGridLineWidth
			            $xaxisFormater
			        },
			        yAxis: [{
			            title: {
			                text: '".$processedFlexFormData['yaxisOneTitle']."'
			                $yaxisOneTitleColor
			            }
		                $categoriesYone
			            $yaxisOneGridLineWidth
			            $yaxisOneFormater
			            $yaxisOneAdditional
			        }";

        if($processedFlexFormData['yaxisTwoTitle'] <> "")
        {
            $jsCode .= ",{
				title: {
			         text: '".$processedFlexFormData['yaxisTwoTitle']."'
			         $yaxisTwoTitleColor
			        },
			    opposite: true
			    $categoriesYtwo
			    $yaxisTwoGridLineWidth
			    $yaxisTwoFormater
			    $yaxisTwoAdditional
			    }

			";
        }

        $jsCode .= "
			        ],
			        series: [";

        foreach ( $a_serieObjects as $serie ) {
            $color = (isset($serie['color']) && $serie['color'] != "" ) ? ',color: "' . $serie['color'] . '"' : '';
            $jsCode .= "
				{
					name: '".$serie['title']."',
					data: [".$serie['data']."],
					dashStyle: '".$serie['dashStyle']."',
					yAxis: ".$serie['yAxis']."
					$color
				";

            if ( $serie['type'] <> "" ) {
                $jsCode .= ",
					type: '".$serie['type']."'";
            }

            if ( $serie['additionalCode'] <> "" ) {
                $jsCode .= ",
				".$serie['additionalCode'];
            }

            $jsCode .= "
				},
			";
        }
        $jsCode = rtrim($jsCode,", \t\n");

        $jsCode .= "]}}";

        // This will be available in fluid:
        $processedData['chartData']= $jsCode;

        return $processedData;
    }

}
