<?php
if (!defined ('TYPO3_MODE')) {
	die ('Access denied.');
}

$tcaTtContent = [
    'types' => [
        'botocharts_chart' => [
            'showitem' =>
            '--palette--;LLL:EXT:cms/locallang_ttc.xlf:palette.general;general,
                    header,
                    --linebreak--,
                    pi_flexform,
                --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,
                    --palette--;;language,
                --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:access,
                    --palette--;;hidden,
                    --palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.access;access,
				--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.extended,
                '
        ]
    ],
    'columns' => [
        'CType' => [
            'config' => [
                'items' => [
                    'botocharts_chart' => [
                        'LLL:EXT:boto_charts/Resources/Private/Language/locallang_db.xlf:tx_botocharts_CE',
                        // Name des Inhaltselementes
                        'botocharts_chart',
                        // TCA Name des Inhaltselementes
                        'EXT:boto_charts/Resources/Public/Icons/ce_icon.gif'
                        // Bild des Inhaltelementes
                    ]
                ]
            ]
        ]
    ]
];

\TYPO3\CMS\Core\Utility\ArrayUtility::mergeRecursiveWithOverrule($GLOBALS['TCA']['tt_content'], $tcaTtContent);

?>