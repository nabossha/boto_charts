# **************************************************
# Add the slider to the "New Content Element Wizard"
# **************************************************
mod.wizards.newContentElement.wizardItems.common {
	elements {
        botocharts_chart {
            icon = EXT:boto_charts/Resources/Public/Icons/ce_icon.gif
            title = LLL:EXT:boto_charts/Resources/Private/Language/locallang_db.xlf:tx_botocharts_CE
            description = LLL:EXT:boto_charts/Resources/Private/Language/locallang_db.xlf:tx_botocharts_CE_description
            tt_content_defValues {
                CType = botocharts_chart
            }
        }
	}
	show := addToList(botocharts_chart)
}
